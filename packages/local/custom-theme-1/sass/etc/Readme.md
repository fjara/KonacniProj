# custom-theme-1/sass/etc

This folder contains miscellaneous SASS files. Unlike `"custom-theme-1/sass/etc"`, these files
need to be used explicitly.
