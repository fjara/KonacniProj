# custom-theme-1/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    custom-theme-1/sass/etc
    custom-theme-1/sass/src
    custom-theme-1/sass/var
