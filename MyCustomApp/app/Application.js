/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('MyApp.Application', {
    extend: 'Ext.app.Application',

    name: 'MyApp',


    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    launch: function(){
       // debugger;
        if ( localStorage.getItem("token")==="logged"){ //ako nije ulogovan idi na home
            this.redirectTo('projects', false);
        }
        else {
         /*   var token = Ext.util.History.getToken();
            if (token === 'login') {
                this.redirectTo('home', false);
            }
            else {
                this.redirectTo('home',false); //ovde zeza
            }
            */
         //ako je ulogovan idiodmah na njegove projekte
            this.redirectTo('home', false);


        }

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
