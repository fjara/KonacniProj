Ext.define('MyApp.view.projects.ProjectsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.projects',

    fields: [
    {
        name: 'username',
        validators: 'presence'
    },
    {
        name: 'password',
        validators: 'presence'
    }],

    data: {
        applocale: {
            username: "Username",
            password: "Password",
            back: "Back",
            reset: "Reset",
            projects: "Projects",
            add: "Add Project",
            logout: "",
            search: "Search Projects:",
            delete: "Delete",
            cancelDelete: "Done Deleting"
        }

    }
});