Ext.define('MyApp.view.projects.ProjectsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.projects',

    addHandler: function () {
        this.redirectTo('addProject');
    },

    listen:{
        component: {
            '[reference=searchBox]': {
                change: 'setComboState'
            },
        }
    },

    setComboState: function(value1, newValue){
        var dugmeDelete = true;
        var dugmeNeDelete = false;
        var isDeleteModeOn = this.lookupReference('deleteProjectDone').getHidden();
        if (isDeleteModeOn === false){
            var dugmeDelete = false;
            var dugmeNeDelete = true;
        } else {
            var dugmeDelete = true;
            var dugmeNeDelete = false;
        }
        Ext.Ajax.request({
            scope: this,
            url: 'http://localhost:8080/projects',
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                var forma = this.lookupReference('projectsPanel');
                forma.removeAll();
                var response = Ext.decode(response.responseText);

                var children = [];
                var  project1 = newValue;
                Ext.each(response, function (dataItem) {
                    //create child

                    if (dataItem.name.includes(project1))
                    {children.push({

                        xtype: 'panel',
                        cls:'button-add-project',
                        items: [
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            items: [
                            {
                                xtype: 'button',
                                text: dataItem.name,
                                cls: 'projects-button',
                                reference: 'dugmePro',

                                listeners:{
                                    tap:'onClick'
                                },
                                width: '96%',
                                height: '60px',
                                hidden: dugmeNeDelete,
                            },
                            {
                                xtype: 'button',
                                text: dataItem.name,
                                cls: 'projects-button',
                                reference: 'dugmeProDel',

                                listeners:{
                                    tap:'onClickDelete',
                                },
                                width: '96%',
                                height: '60px',
                                hidden: dugmeDelete,
                            }]
                        },
                        {
                            xtype: 'label',
                            html: dataItem.id,
                            reference:dataItem.name,
                            hidden:'true'
                        }]
                    });
                    }
                });

                //add children to panel at once
                forma.add(children);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    onShow: function () {

        var adminChecker = localStorage.getItem("adminId");
        var adminCheckerNumber = localStorage.getItem("adminIdTest");
        if(adminChecker === adminCheckerNumber){
            this.lookupReference('addProjectHandler').setHidden(false);
            this.lookupReference('deleteProject').setHidden(false);
        } else {
            this.lookupReference('addProjectHandler').setHidden(true);
            this.lookupReference('deleteProject').setHidden(true);
        }

        var token = localStorage.getItem("token1");
        var c = this.lookupReference('userLogged');

        if (c == "Nepostojeci korisnik") { //ako niko nije ulogovan a neko je preko linka u browseru otisao na projects
            this.redirectTo('home', false);
        }

        c.setHtml("Hello, " + token);

        Ext.Ajax.request({
            scope: this,
            url: 'http://localhost:8080/projects',
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                var forma = this.lookupReference('projectsPanel');
                forma.removeAll();
                var response = Ext.decode(response.responseText);

                var children = [];

                Ext.each(response, function (dataItem) {
                    //create child
                    children.push({

                        xtype: 'panel',
                        cls:'button-add-project',
                        items: [
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            items: [
                            {
                                xtype: 'button',
                                text: dataItem.name,
                                cls: 'projects-button',
                                reference: 'dugmePro',

                                listeners:{
                                    tap:'onClick',
                                },
                                width: '96%',
                                height: '60px'
                            },
                            {
                                xtype: 'button',
                                text: dataItem.name,
                                cls: 'projects-button',
                                reference: 'dugmeProDel',

                                listeners:{
                                    tap:'onClickDelete',
                                },
                                width: '96%',
                                height: '60px',
                                hidden: true,
                            }]
                        },
                        {
                            xtype: 'label',
                            html: dataItem.id,
                            reference:dataItem.name,
                            hidden:'true'
                        }]
                    });
                });

                //add children to panel at once
                forma.add(children);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    logout: function () {
        localStorage.setItem('token', "notlogged"); //vise nije ulogovan
        localStorage.setItem('token1', "Nepostojeci korisnik");
        this.redirectTo("home");
    },

    onClick: function(dugme){
        imeProj=dugme.getText();
        idProj = this.lookupReference(imeProj).getHtml();
        var forma = this.lookupReference('searchBox');
        forma.reset();
        this.redirectTo('tasks/'+idProj+'_'+imeProj);
    },

    deleteProject: function(){
        this.lookupReference('dugmePro').setHidden(true);
        this.lookupReference('deleteProject').setHidden(true);
        this.lookupReference('addProjectHandler').setHidden(true);
        //this.lookupReference('searchBox').setHidden(true);
        this.lookupReference('dugmeProDel').setHidden(false);
        this.lookupReference('deleteProjectDone').setHidden(false);
    },

    deleteProjectDone: function(){
        this.lookupReference('dugmePro').setHidden(false);
        var adminChecker = localStorage.getItem("adminId");
        var adminCheckerNumber = localStorage.getItem("adminIdTest");
        if(adminChecker === adminCheckerNumber){
            this.lookupReference('addProjectHandler').setHidden(false);
            this.lookupReference('deleteProject').setHidden(false);
        } else {
            this.lookupReference('addProjectHandler').setHidden(true);
            this.lookupReference('deleteProject').setHidden(true);
        }
        this.lookupReference('searchBox').setHidden(false);
        this.lookupReference('dugmeProDel').setHidden(true);
        this.lookupReference('deleteProjectDone').setHidden(true);
        var forma = this.lookupReference('searchBox');
        forma.reset();
    },

    onClickDelete: function(dugme){
        var forma = this.lookupReference('projectsPanel');
        // var dugmeProVar = this.lookupReference('dugmePro');
        // var deleteProjectVar = this.lookupReference('deleteProject');

        Ext.Msg.confirm('Delete Project', 'Are you sure?', function(btn){
            if(btn === 'yes'){
                imeProj=dugme.getText();

                Ext.Ajax.request({
                    scope: this,
                    url: 'http://localhost:8080//projects/' +imeProj,
                    method: 'DELETE',

                    header: Ext.encode({
                        "name": imeProj
                    }),

                    defaultHeaders: {
                        'Content-Type': 'application/json '
                    },

                    success: function (response, opts) {

                        Ext.Ajax.request({
                            scope: this,
                            url: 'http://localhost:8080/projects',
                            method: 'GET',

                            defaultHeaders: {
                                'Content-Type': 'application/json '
                            },

                            success: function (response, opts) {

                                forma.removeAll();
                                var response = Ext.decode(response.responseText);

                                var children = [];

                                Ext.each(response, function (dataItem) {
                                    //create child
                                    children.push({

                                        xtype: 'panel',
                                        cls:'button-add-project',
                                        items: [
                                            {
                                                xtype: 'panel',
                                                layout: {
                                                    type: 'hbox',
                                                    pack: 'center'
                                                },
                                                items: [
                                                    {
                                                        xtype: 'button',
                                                        text: dataItem.name,
                                                        cls: 'projects-button',
                                                        reference: 'dugmePro',

                                                        listeners:{
                                                            tap:'onClick',
                                                        },
                                                        width: '96%',
                                                        height: '60px',
                                                        hidden: true,
                                                    },
                                                    {
                                                        xtype: 'button',
                                                        text: dataItem.name,
                                                        cls: 'projects-button',
                                                        reference: 'dugmeProDel',

                                                        listeners:{
                                                            tap:'onClickDelete',
                                                        },
                                                        width: '96%',
                                                        height: '60px',
                                                        hidden: false,
                                                    }]
                                            },
                                            {
                                                xtype: 'label',
                                                html: dataItem.id,
                                                reference:dataItem.name,
                                                hidden:'true'
                                            }]
                                    });
                                });

                                //add children to panel at once
                                forma.add(children);
                            },

                            failure: function (response, opts) {
                                console.log('server-side failure with status code ' + response.status);
                            }
                        });
                    },

                    failure: function (response, opts) {
                        console.log('server-side failure with status code ' + response.status);
                    }
                });

            }
            else{
            }
        });

        this.lookupReference('dugmePro').setHidden(true);
        this.lookupReference('deleteProject').setHidden(true);
        this.lookupReference('addProjectHandler').setHidden(true);
        //this.lookupReference('searchBox').setHidden(true);
        this.lookupReference('dugmeProDel').setHidden(false);
        this.lookupReference('deleteProjectDone').setHidden(false);
    }
});