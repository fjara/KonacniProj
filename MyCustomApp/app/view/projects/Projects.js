Ext.define('MyApp.view.projects.Projects', {
    extend: 'Ext.form.Panel',
    bind: {
        title: '{applocale.projects}'
    },

    id: 'projectsForm',
    renderTo: 'projectsId',
    controller: 'projects',

    viewModel: 'projects',

    xtype: 'projects',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',

    requires: [
        'MyApp.view.projects.ProjectsModel',
        'MyApp.view.projects.ProjectsController',
    ],

    listeners: {
        show: 'onShow'

    },

    items: [
    {
        xtype: 'textfield',
        reference: 'searchBox',
        bind: {
            label: '{applocale.search}',
        },
    }, {
        xype: 'container',
        reference: 'projectsPanel',
        id: 'projectsPanel'
    }],

    buttons: [
    {
        reference: 'deleteProject',
        handler: 'deleteProject',
        docked: 'left',
        bind: {
            text: '{applocale.delete}',
        },
        hidden: true,
        iconCls: 'x-fa fa-trash',
        iconAlign: 'left'

    },
    {
        reference: 'deleteProjectDone',
        handler: 'deleteProjectDone',
        docked: 'left',
        bind: {
            text: '{applocale.cancelDelete}',
        },
        hidden: true,
        iconCls: 'x-fa fa-times',
        iconAlign: 'left'
    },
    {
        bind: {
            text: '{applocale.add}',
        },
        docked: 'right',
        reference: 'addProjectHandler',
        handler: 'addHandler',
        iconCls: 'x-fa fa-plus',
        iconAlign: 'right',
        hidden: true
    }],

    header: {
        titlePosition: 0,
        items: [{
            xtype: 'label',
            reference: 'userLogged',
        },
        {
            xtype: 'button',
            bind: {
                text: '{applocale.logout}'
            },
            docked: 'right',
            handler: 'logout',
            iconCls: 'x-fa fa-sign-out',
            iconAlign: 'right',
        }]
    }
});