Ext.define('MyApp.view.tasksDetails.TasksDetailsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tasksDetails',

    data: {
        applocale: {
            tasksDetails:"Task Details",
            summary:"Summary: ",
            assignee:"Assignee: ",
            type:"Type: ",
            storyPoints:"Story Points: ",
            issuer:"Issuer: ",
            description:"Description: ",
            dugmeLocalOk:"OK",
            dugmeLocalCancel:"Cancel",
            logout: "",
            back: "Back",
            dugmeLocalEdit:"Edit",
            status:'Status:',
            issueTime:"Task issued at: ",
            delete: "Delete Task"
            //da mogu push
        }
    }
});