Ext.define('MyApp.view.tasksDetails.TasksDetailsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tasksDetails',

    backHandler: function () {
        history.back();
    },


    onShow: function () {

        var deleteVisible = true;
        var adminChecker = localStorage.getItem("adminId");
        var adminCheckerNumber = localStorage.getItem("adminIdTest");
        if(adminChecker === adminCheckerNumber){
            deleteVisible = false;
        } else {
            deleteVisible = true;
        }

        var adminChecker = localStorage.getItem("adminId");
        var adminCheckerNumber = localStorage.getItem("adminIdTest");
        if(adminChecker === adminCheckerNumber){
            this.lookupReference('editTask').setHidden(false);
            this.lookupReference('editTaskNoId').setHidden(true);
        } else {
            this.lookupReference('editTask').setHidden(true);
            this.lookupReference('editTaskNoId').setHidden(false);
        }

        var c = this.lookupReference('userLogged');

        if (c == "Nepostojeci korisnik") { //ako niko nije ulogovan a neko je preko linka u browseru otisao na projects
            this.redirectTo('home', false);
        }

        var token = localStorage.getItem("token1");

        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var taskId = urlSplitIds[urlSplitIds.length - 1];
        var projectName = urlSplitIds[urlSplitIds.length - 2];
        var projectId = urlSplitIds[urlSplitIds.length - 3];

        var assigneeStore = [];

        var storeEditTask = Ext.create('Ext.data.Store',{
            fields : ['abbr','name'],
            data: assigneeStore,
            paging : false
        });

        Ext.Ajax.request({

            url: 'http://localhost:8080/users',
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json'
            },

            success: function (response, opts) {
                var usersJson = Ext.decode(response.responseText);

                for (var i = 0; i < usersJson.length; i++){
                    assigneeStore.push({
                        abbr: usersJson[i].username,
                        name: usersJson[i].firstName+' '+usersJson[i].lastName
                    });
                }
                storeEditTask.loadData(assigneeStore);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });

        Ext.Ajax.request({
            scope: this,
            url: 'http://localhost:8080/tickets/' + taskId,
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                // debugger;
                var forma = Ext.getCmp('tasksDetails');
                forma.removeAll();
                var response = Ext.decode(response.responseText);

                var children = [];

                Ext.each(response, function (dataItem) {

                    var timeSplitOne = dataItem.createdDate.split("T");
                    var timeSplitTwo = timeSplitOne[1].split('.');
                    var timeSplitThree = timeSplitTwo[0].split(':');
                    var timezoneOffset = new Date().getTimezoneOffset();
                    var timezoneDifference = timezoneOffset/60;
                    timeSplitThree[0] = timeSplitThree[0]-timezoneDifference;
                    var timeIssueCreated = timeSplitThree[0]+':'+timeSplitThree[1];
                    var timeSplitFour = timeSplitOne[0].split('-');
                    var dateIssueCreated = timeSplitFour[2]+'/'+timeSplitFour[1]+'/'+timeSplitFour[0];
                    var createTime = timeIssueCreated+' - '+dateIssueCreated;

                    Ext.Ajax.request({

                        url: 'http://localhost:8080/tickets/' + dataItem.id + '/userAssignee',
                        method: 'GET',

                        defaultHeaders: {
                            'Content-Type': 'application/json '
                        },

                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            assigneeCurrent = obj.firstName + ' ' + obj.lastName;
                            localStorage.setItem('currentAssigneTask', assigneeCurrent);

                        },
                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });

                    var assigneeCurrent = localStorage.getItem("currentAssigneTask");


                    //create child
                    children.push({

                        xtype: 'panel',
                        items: [
                        {
                            xtype: 'panel',
                            cls: 'task-detail-panel',
                            layout: {
                                type: 'vbox'
                            },
                            items: [
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox'
                                },
                                items: [
                                    {
                                        xtype: 'label',
                                        bind: {
                                            html: '{applocale.summary}',
                                        },
                                        reference: 'summaryLabelModel'
                                    },
                                    {
                                        xtype: 'label',
                                        html: dataItem.summary,
                                        reference: 'summaryLabel'
                                    },
                                    {
                                        xtype: 'textareafield',
                                        value: dataItem.summary,
                                        maxLength: 200,
                                        height: '60px',
                                        reference: 'summaryText',
                                        hidden: true,
                                    }]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox',
                                },
                                items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                    },
                                    items: [
                                    {
                                        xtype: 'label',
                                        bind: {
                                            html: '{applocale.assignee}',
                                        },
                                        reference: 'assigneeLabelModel'
                                    },
                                    {
                                        xtype: 'label',
                                        html: assigneeCurrent,
                                        reference: 'assigneeLabel'
                                    }]
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'assigneeText',
                                    id: 'assigneeText',
                                    displayField: 'name',
                                    valueField: 'abbr',
                                    hidden: true,
                                    store: storeEditTask,
                                }]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox',
                                },
                                items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                    },
                                    items: [
                                    {
                                        xtype: 'label',
                                        bind: {
                                            html: '{applocale.type}',
                                        },
                                        reference: 'typeLabelModel'
                                    },
                                    {
                                        xtype: 'label',
                                        html: dataItem.ticketType,
                                        reference: 'typeLabel',
                                    }]
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'typeText',
                                    displayField: 'name',
                                    valueField: 'abbr',
                                    hidden: true,
                                    store: [
                                        {abbr: 'TASK', name: 'TASK'},
                                        {abbr: 'BUG', name: 'BUG'},
                                        {abbr: 'IMPROVEMENT', name: 'IMPROVEMENT'},
                                        {abbr: 'NEW_FEATURE', name: 'NEW_FEATURE'},
                                    ],
                                }]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox',
                                },
                                items: [{
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                    },
                                    items: [
                                    {
                                        xtype: 'label',
                                        bind: {
                                            html: '{applocale.status}',
                                        },
                                        reference: 'statusLabelModel'
                                    },
                                    {
                                        xtype: 'label',
                                        html: dataItem.status,
                                        reference: 'statusLabel',
                                    }]
                                },
                                {
                                    xtype: 'combobox',
                                    reference: 'statusText',
                                    displayField: 'name',
                                    valueField: 'abbr',
                                    hidden: true,
                                    store: [
                                        {abbr: 'TO_DO', name: 'TO_DO'},
                                        {abbr: 'IN_PROGRESS', name: 'IN_PROGRESS'},
                                        {abbr: 'RESOLVED', name: 'RESOLVED'},
                                    ]
                                }]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox'
                                },
                                items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                    },
                                    items: [
                                    {
                                        xtype: 'label',
                                        bind: {
                                            html: '{applocale.storyPoints}',
                                        },
                                        reference: 'storypointsLabelModel'
                                    },
                                    {
                                        xtype: 'label',
                                        html: dataItem.points,
                                        reference: 'storyLabel',
                                    }]
                                },
                                {
                                    xtype: 'numberfield',
                                    value: dataItem.points,
                                    reference: 'storyText',
                                    hidden: true
                                }]
                            },
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox'
                                },
                                items: [
                                {
                                    xtype: 'label',
                                    bind: {
                                        html: '{applocale.issueTime}',
                                    },
                                    reference: 'issueTimeLabelModel'
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'vbox',
                                    },
                                    items: [
                                    {
                                        xtype: 'label',
                                        html: createTime,
                                        reference: 'issueTimeLabel',
                                    }]
                                }]
                            },
                            // {
                            //     xtype: 'panel',
                            //     layout: {
                            //         type: 'hbox'
                            //     },
                            //     items: [
                            //     {
                            //         xtype: 'label',
                            //         bind: {
                            //             html: '{applocale.issuer}',
                            //         },
                            //         reference: 'issuerLabelModel'
                            //     },
                            //     {
                            //         type: 'label',
                            //         html: dataItem.issuerId,
                            //         reference: 'issuerLabel',
                            //     }]
                            // },
                            {
                                xtype: 'panel',
                                layout: {
                                    type: 'vbox'
                                },
                                items: [
                                {
                                    xtype: 'label',
                                    bind: {
                                        html: '{applocale.description}',
                                    },
                                    reference: 'descriptionLabelModel'
                                },
                                {
                                    xtype: 'label',
                                    html: dataItem.description,
                                    reference: 'descrLabel',
                                },
                                {
                                    xtype: 'textareafield',
                                    value: dataItem.description,
                                    reference: 'descrText',
                                    hidden: true,
                                }]
                            },
                            {
                                xtype: 'panel',
                                layout: {
                                    type: 'hbox',
                                    pack: 'end'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        reference: 'deleteTask',
                                        handler: 'deleteTask',
                                        bind: {
                                            text: '{applocale.delete}',
                                        },
                                        hidden: deleteVisible,
                                        iconCls: 'x-fa fa-trash',
                                        iconAlign: 'right'
                                    }]
                            }]
                        },
                        {
                            xtype: 'label',
                            hidden: true,
                            name: 'idTask',
                            // html: dataItem.taskId,
                        }]
                    });
                });
                //add children to panel at once
                forma.add(children[0]);

            },
            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    logout: function () {
        localStorage.setItem('token', "notlogged"); //vise nije ulogovan
        localStorage.setItem('token1', "Nepostojeci korisnik");
        this.redirectTo("home");
    },

    editTask:function(){
        this.lookupReference('summaryLabel').setHidden(true);
        this.lookupReference('summaryText').setHidden(false);
        this.lookupReference('descrLabel').setHidden(true);
        this.lookupReference('descrText').setHidden(false);
        this.lookupReference('storyLabel').setHidden(true);
        this.lookupReference('storyText').setHidden(false);
        this.lookupReference('typeLabel').setHidden(true);
        this.lookupReference('typeText').setHidden(false);
        this.lookupReference('statusLabel').setHidden(true);
        this.lookupReference('statusText').setHidden(false);
        // this.lookupReference('issuerLabelModel').setHidden(true);
        // this.lookupReference('issuerLabel').setHidden(true);
        this.lookupReference('assigneeLabel').setHidden(true);
        this.lookupReference('assigneeText').setHidden(false);
        this.lookupReference('issueTimeLabel').setHidden(true);
        this.lookupReference('issueTimeLabelModel').setHidden(true);
        this.lookupReference('editTask').setHidden(true);
        this.lookupReference('back').setHidden(true);
        this.lookupReference('okTask').setHidden(false);
        this.lookupReference('cancelTask').setHidden(false);
        this.lookupReference('typeText').setValue(this.lookupReference('typeLabel').getHtml());
        this.lookupReference('statusText').setValue(this.lookupReference('statusLabel').getHtml());
        this.lookupReference('assigneeText').setValue(this.lookupReference('assigneeLabel').getHtml());
    },

    editTaskNoId:function(){
        this.lookupReference('statusLabel').setHidden(true);
        this.lookupReference('statusText').setHidden(false);
        this.lookupReference('editTaskNoId').setHidden(true);
        this.lookupReference('back').setHidden(true);
        this.lookupReference('okTaskNoId').setHidden(false);
        this.lookupReference('cancelTaskNoId').setHidden(false);
        this.lookupReference('statusText').setValue(this.lookupReference('statusLabel').getHtml());
    },

    cancelTask:function(){
        this.lookupReference('summaryLabel').setHidden(false);
        this.lookupReference('summaryText').setHidden(true);
        this.lookupReference('descrLabel').setHidden(false);
        this.lookupReference('descrText').setHidden(true);
        this.lookupReference('storyLabel').setHidden(false);
        this.lookupReference('storyText').setHidden(true);
        this.lookupReference('typeLabel').setHidden(false);
        this.lookupReference('typeText').setHidden(true);
        this.lookupReference('statusLabel').setHidden(false);
        this.lookupReference('statusText').setHidden(true);
        // this.lookupReference('issuerLabelModel').setHidden(false);
        // this.lookupReference('issuerLabel').setHidden(false);
        this.lookupReference('assigneeLabel').setHidden(false);
        this.lookupReference('assigneeText').setHidden(true);
        this.lookupReference('issueTimeLabel').setHidden(false);
        this.lookupReference('issueTimeLabelModel').setHidden(false);
        this.lookupReference('editTask').setHidden(false);
        this.lookupReference('back').setHidden(false);
        this.lookupReference('okTask').setHidden(true);
        this.lookupReference('cancelTask').setHidden(true);
        this.lookupReference('summaryText').setValue(this.lookupReference('summaryLabel').getHtml());
        this.lookupReference('descrText').setValue(this.lookupReference('descrLabel').getHtml());
        this.lookupReference('storyText').setValue(this.lookupReference('storyLabel').getHtml());
        this.lookupReference('typeText').setValue(this.lookupReference('typeLabel').getHtml());
        this.lookupReference('assigneeText').setValue(this.lookupReference('assigneeLabel').getHtml());
        this.lookupReference('statusText').setValue(this.lookupReference('statusLabel').getHtml());
    },

    cancelTaskNoId:function(){
        this.lookupReference('statusLabel').setHidden(false);
        this.lookupReference('statusText').setHidden(true);
        this.lookupReference('editTaskNoId').setHidden(false);
        this.lookupReference('back').setHidden(false);
        this.lookupReference('okTaskNoId').setHidden(true);
        this.lookupReference('cancelTaskNoId').setHidden(true);
        this.lookupReference('statusText').setValue(this.lookupReference('statusLabel').getHtml());
    },

    okTask:function(){
        var summary =  this.lookupReference('summaryText').getValue();
        var descr =  this.lookupReference('descrText').getValue();
        var story =  this.lookupReference('storyText').getValue();
        var selectedType = this.lookupReference('typeText').getValue();
        var assignee = this.lookupReference('assigneeText').getValue();
        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var taskId = urlSplitIds[urlSplitIds.length - 1];
        var projectId = urlSplitIds[urlSplitIds.length - 2];
        var status = this.lookupReference('statusText').getValue();

        Ext.Ajax.request({
            scope: this,
            url: 'http://localhost:8080/tickets',
            method: 'PUT',

            params: Ext.encode({
                "description": descr,
                "id": taskId,
                "points": story,
                "status": status,
                "summary": summary,
                "ticketType": selectedType,
                "username": assignee
            }),

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                var response = Ext.decode(response.responseText);

                this.lookupReference('summaryLabel').setHidden(false);
                this.lookupReference('summaryText').setHidden(true);
                this.lookupReference('descrLabel').setHidden(false);
                this.lookupReference('descrText').setHidden(true);
                this.lookupReference('storyLabel').setHidden(false);
                this.lookupReference('storyText').setHidden(true);
                this.lookupReference('typeLabel').setHidden(false);
                this.lookupReference('typeText').setHidden(true);
                this.lookupReference('statusLabel').setHidden(false);
                this.lookupReference('statusText').setHidden(true);
                // this.lookupReference('issuerLabelModel').setHidden(false);
                // this.lookupReference('issuerLabel').setHidden(false);
                this.lookupReference('assigneeLabel').setHidden(false);
                this.lookupReference('assigneeText').setHidden(true);
                this.lookupReference('issueTimeLabel').setHidden(false);
                this.lookupReference('issueTimeLabelModel').setHidden(false);
                this.lookupReference('editTask').setHidden(false);
                this.lookupReference('back').setHidden(false);
                this.lookupReference('okTask').setHidden(true);
                this.lookupReference('cancelTask').setHidden(true);
                this.lookupReference('summaryLabel').setHtml(summary);
                this.lookupReference('descrLabel').setHtml(descr);
                this.lookupReference('storyLabel').setHtml(story);
                this.lookupReference('typeLabel').setHtml(selectedType);
                this.lookupReference('statusLabel').setHtml(status);
                var assigneeChange = this.lookupReference('assigneeText').getSelection().getData();
                this.lookupReference('assigneeLabel').setHtml(assigneeChange.name);

            },

            failure: function (response, opts) {
                this.lookupReference('summaryLabel').setHidden(false);
                this.lookupReference('summaryText').setHidden(true);
                this.lookupReference('descrLabel').setHidden(false);
                this.lookupReference('descrText').setHidden(true);
                this.lookupReference('storyLabel').setHidden(false);
                this.lookupReference('storyText').setHidden(true);
                this.lookupReference('typeLabel').setHidden(false);
                this.lookupReference('typeText').setHidden(true);
                this.lookupReference('statusLabel').setHidden(false);
                this.lookupReference('statusText').setHidden(true);
                // this.lookupReference('issuerLabelModel').setHidden(false);
                // this.lookupReference('issuerLabel').setHidden(false);
                this.lookupReference('assigneeLabel').setHidden(false);
                this.lookupReference('assigneeText').setHidden(true);
                this.lookupReference('issueTimeLabel').setHidden(false);
                this.lookupReference('issueTimeLabelModel').setHidden(false);
                this.lookupReference('editTask').setHidden(false);
                this.lookupReference('back').setHidden(false);
                this.lookupReference('okTask').setHidden(true);
                this.lookupReference('cancelTask').setHidden(true);

                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    okTaskNoId:function(){
        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var taskId = urlSplitIds[urlSplitIds.length - 1];
        var projectId = urlSplitIds[urlSplitIds.length - 2];
        var status = this.lookupReference('statusText').getValue();

        Ext.Ajax.request({
            scope: this,
            url: 'http://localhost:8080/tickets/status',
            method: 'PUT',

            params: Ext.encode({
                "ticket_id": taskId,
                "status": status,
            }),

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                var response = Ext.decode(response.responseText);

                this.lookupReference('statusLabel').setHidden(false);
                this.lookupReference('statusText').setHidden(true);
                this.lookupReference('editTaskNoId').setHidden(false);
                this.lookupReference('back').setHidden(false);
                this.lookupReference('okTaskNoId').setHidden(true);
                this.lookupReference('cancelTaskNoId').setHidden(true);
                this.lookupReference('statusLabel').setHtml(status);
            },

            failure: function (response, opts) {
                this.lookupReference('statusLabel').setHidden(false);
                this.lookupReference('statusText').setHidden(true);
                this.lookupReference('editTaskNoId').setHidden(false);
                this.lookupReference('back').setHidden(false);
                this.lookupReference('okTaskNoId').setHidden(true);
                this.lookupReference('cancelTaskNoId').setHidden(true);

                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    deleteTask: function(){
        Ext.Msg.confirm('Delete Task', 'Are you sure?', function(btn){
            if(btn === 'yes'){
                var url = window.location.href;
                var urlSplit = url.split("/");
                var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
                var taskId = urlSplitIds[urlSplitIds.length - 1];
                var projectName = urlSplitIds[urlSplitIds.length - 2];
                var projectId = urlSplitIds[urlSplitIds.length - 3];
                // da mogu push

                Ext.Ajax.request({
                    scope: this,
                    url: 'http://localhost:8080/tickets/' + taskId,
                    method: 'DELETE',

                    header: Ext.encode({
                        "ticketId": taskId
                    }),

                    defaultHeaders: {
                        'Content-Type': 'application/json '
                    },

                    success: function (response, opts) {
                        history.back();
                    },

                    failure: function (response, opts) {
                        console.log('server-side failure with status code ' + response.status);
                    }
                });
            }
            else{
            }
        });
    }
});