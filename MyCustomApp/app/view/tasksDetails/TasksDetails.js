Ext.define('MyApp.view.tasksDetails.TasksDetails', {
    extend: 'Ext.Panel',

    bind: {
        title: '{applocale.tasksDetails}'
    },
    //da mogu push

    id: 'tasksDetails',
    renderTo: 'tasksDetailsId',
    controller: 'tasksDetails',
    viewModel: 'tasksDetails',

    xtype: 'tasksDetails',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',

    requires: [
        'MyApp.view.tasksDetails.TasksDetailsModel',
        'MyApp.view.tasksDetails.TasksDetailsController',
    ],

    listeners: {
        show: 'onShow'
    },

    buttons: [
    {
        bind: {
            text: '{applocale.back}',
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left',
    },
    {
        bind: {
            text: '{applocale.dugmeLocalEdit}',
        },
        docked: 'right',
        reference: 'editTask',
        handler: 'editTask',
        iconCls: 'x-fa fa-edit',
        iconAlign: 'right',
        hidden: true
    },
    {
        bind: {
            text: '{applocale.dugmeLocalEdit}',
        },
        docked: 'right',
        reference: 'editTaskNoId',
        handler: 'editTaskNoId',
        iconCls: 'x-fa fa-edit',
        iconAlign: 'right',
        hidden: true,
    },
    {
        docked: 'right',
        reference: 'okTask',
        handler: 'okTask',
        bind: {
            text: '{applocale.dugmeLocalOk}',
        },
        hidden: true,
        iconCls: 'x-fa fa-check',
        iconAlign: 'right'
    },
    {
        docked: 'right',
        reference: 'okTaskNoId',
        handler: 'okTaskNoId',
        bind: {
            text: '{applocale.dugmeLocalOk}',
        },
        hidden: true,
        iconCls: 'x-fa fa-check',
        iconAlign: 'right'
    },
    {
        docked: 'left',
        reference: 'cancelTask',
        handler: 'cancelTask',
        bind: {
            text: '{applocale.dugmeLocalCancel}',
        },
        hidden: true,
        iconCls: 'x-fa fa-times',
        iconAlign: 'left'
    },
    {
        docked: 'left',
        reference: 'cancelTaskNoId',
        handler: 'cancelTaskNoId',
        bind: {
            text: '{applocale.dugmeLocalCancel}',
        },
        hidden: true,
        iconCls: 'x-fa fa-times',
        iconAlign: 'left'
        }],

    header: {
        titlePosition: 0,
        items: [
        {
            xtype: 'button',
            bind: {
                text: '{applocale.logout}'
            },
            docked: 'right',
            handler: 'logout',
            iconCls: 'x-fa fa-sign-out',
            iconAlign: 'right',
        }]
    }
});