Ext.define('MyApp.view.addProject.AddProject', {
    extend: 'Ext.form.Panel',

    bind: {
        title: '{applocale.addProject}'
    },

    id: 'addProjectForm',
    renderTo: 'addProjectId',
    controller: 'addProject',

    viewModel: 'addProject',
    reference: 'addProject',

    requires: [
        'MyApp.view.addProject.AddProjectModel',
        'MyApp.view.addProject.AddProjectController',
    ],
    xtype: 'addProject',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',

    buttons: [
    {
        bind: {
            text: '{applocale.back}',
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left',
    },
    {
        bind: {
            text: '{applocale.addProject}',
        },
        docked: 'right',
        reference: 'addProjectRefHandler',
        handler: 'addHandler',
        iconCls: 'x-fa fa-plus',
        iconAlign: 'right',
    }],

    items: [
        {
            xtype: 'textfield',
            bind: {
                label: '{applocale.projectName}'
            },
            id: 'projectName',
            required: true
        },
    ],

    header:{
        titlePosition: 0,
        items:[
        {
            xtype: 'button',
            bind: {
                text: '{applocale.logout}'
            },
            docked: 'right',
            handler: 'logout',
            iconCls: 'x-fa fa-sign-out',
            iconAlign: 'right',
        }]
    }
});