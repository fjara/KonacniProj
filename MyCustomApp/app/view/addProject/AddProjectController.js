Ext.define('MyApp.view.addProject.AddProjectController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.addProject',

    backHandler: function () {
        history.back();
        var forma = Ext.getCmp('addProjectForm');
        forma.reset();
    },

    addHandler: function () {
        var forma = Ext.getCmp('addProjectForm');

        forma.validate();
        if (forma.isValid()) {

            var token = localStorage.getItem("token1");
            var projectName = Ext.getCmp('projectName').getValue();

            Ext.Ajax.request({
                scope: this,
                url: 'http://localhost:8080/projects',
                method: 'POST',

                params: Ext.encode({
                    "name": projectName,
                }),

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    history.back();
                    forma.reset();
                },

                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        else {
        }
    },

    logout: function () {
        localStorage.setItem('token', "notlogged"); //vise nije ulogovan
        localStorage.setItem('token1', "Nepostojeci korisnik");

        this.redirectTo("home");
    },
});