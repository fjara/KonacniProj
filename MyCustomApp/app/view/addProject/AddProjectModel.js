Ext.define('MyApp.view.addProject.AddProjectModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.addProject',

    fields: [{
        name: 'username',
        validators: 'presence'
    },
        {
            name: 'password',
            validators: 'presence'
        }],


    data: {
        applocale: {
            username: "Username",
            password: "Password",
            back: "Back",
            reset: "Reset",
            addProject: "Add Project",
            projectName: "Project Name",
            logout: "",
        }
    }
});