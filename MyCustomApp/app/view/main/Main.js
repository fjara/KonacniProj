/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit'
    ],

    controller: 'main',
    viewModel: 'main',

    items: [
    {
        xtype: 'login',
        hidden: true,
        reference:'login'
    },{
        xtype: 'register',
        hidden: true,
        reference:'register'
    },
    {
        xtype: 'projects',
        hidden: true,
        reference:'projects'
    },
    {
        xtype: 'addProject',
        hidden: true,
        reference:'addProject'
    },
    {
        xtype: 'home',
        hidden: true,
        reference: 'home'
    },
    {
        xtype: 'tasks',
        hidden: true,
        reference: 'tasks'
    },
    {
        xtype:'addTask',
        hidden:true,
        reference:'addTask'
    },
    {
        xtype:'tasksDetails',
        hidden:true,
        reference:'tasksDetails'
    }]
});
