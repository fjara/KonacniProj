/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    routes: {
        'login': {action:function(){
            this.showHide('login',-1);
        }},
        'register':{action: function(){
            this.showHide('register',-1);
        }},
        'projects': {action:function(){
            this.showHide('projects',-1);
        }},
        'addProject': {action:function(){
            this.showHide('addProject',-1);
        }},
        'home': {action:function(){
            this.showHide('home',-1);
        }},
        'tasks/:id': {action:function(id){
            this.showHide('tasks',id);
        }},
        'addTask/:id': {action:function(id){
            this.showHide('addTask',id);
        }},
        'tasksDetails/:idTask': {
            conditions : {
                ':id' : '([0-9]+)'
            },
            action:function(){
                this.showHide('tasksDetails',-1);
            }
        },
    },

    showHide:function (t,id) {

        this.getView().getActiveItem().hide();
        this.getView().setActiveItem(t);
        if(id!=-1){
            this.getView().getActiveItem().lookupReference('idProj').setHtml(id);
        }
        this.getView().lookup(t).show();
    }
});
