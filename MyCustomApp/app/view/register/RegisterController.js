Ext.define('MyApp.view.register.RegisterController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.register',

    backHandler: function () {
        history.back();
        var forma = Ext.getCmp('registerForm');
        forma.reset();
        this.lookupReference('emailWrong').setHidden(true);
        this.lookupReference('passwordWrong').setHidden(true);
    },

    registerHandler: function () {
        var forma = Ext.getCmp('registerForm');
        forma.validate();

        var emailFormTester = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
        var emailTestResult = emailFormTester.test(this.lookupReference('email').getValue());
        if (emailTestResult === false){
            this.lookupReference('emailWrong').setHidden(false);
        } else {
            this.lookupReference('emailWrong').setHidden(true);
        }
        if (this.lookupReference('password').getValue() === this.lookupReference('repeatedPassword').getValue()) {
            var passwordsMatch = true;
            this.lookupReference('passwordWrong').setHidden(true);
        } else {
            this.lookupReference('passwordWrong').setHidden(false);
        }

        if (forma.isValid()&& emailTestResult === true && passwordsMatch === true) {

            Ext.Ajax.request({
                scope: this,
                url: 'http://localhost:8080//users/register',

                params: Ext.encode(this.getView().getValues()),

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    this.redirectTo('login');
                    forma.reset();
                },

                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        else {
        }
        //this.getView().getValues(); dohvati sva polja
        //localStorage.setItem('logedIn',true);
        //this.redirectTo('login',)Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    }
});