Ext.define('MyApp.view.register.RegisterModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.register',

    /* fields:[
         'sym', 'type', 'src', 'target', 'path'
     ],
 */


    fields: ['firstName', 'lastName', 'username', 'email', 'password', 'repeatedPassword'],
    proxy: {
        type: 'ajax',
        api: {
            read: 'data/get_user',
            update: 'data/update_user'
        },
        reader: {
            type: 'json',
            root: 'users'
        }
    },

    //uradi lokalizaciju

    data: {
        applocale: {
            register: "Register",
            username: 'Username',
            firstname: "First Name",
            lastname: "Last Name",
            email: "Email",
            password: "Password",
            confirm: "Confirm Password",
            back: "Back",
            reset: "Reset",
            emailProblem: 'Invalid email address!',
            passwordProblem: 'Passwords do not match!'
        }
    }
});



