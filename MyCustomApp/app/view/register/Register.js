Ext.define('MyApp.view.register.Register', {
    extend: 'Ext.form.Panel',
    bind: {
        title: '{applocale.register}',
    },

    id: 'registerForm',
    renderTo: 'formId',
    controller: 'register',
    viewModel: 'register',
    // model: 'domaciGrid',
    cls: 'register-form',

    requires: [
        'MyApp.view.register.RegisterModel',
        'MyApp.view.register.RegisterController',
    ],

    xtype: 'register',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',
    //hideAnimation: 'slideOut',

    // header:{
    //     titlePosition: 0,
    // },

    items: [
    {
        xtype: 'textfield',
        name: 'username',
        reference: 'user',
        required: true,
        maxLength: 16,
        bind: {
            label: '{applocale.username}',
        },
    },
    {
        xtype: 'textfield',
        name: 'firstName',
        required: true,
        reference: 'firstName',
        bind: {
            label: '{applocale.firstname}',
        },
    },
    {
        xtype: 'textfield',
        name: 'lastName',
        required: true,
        reference: 'lastName',
        bind: {
            label: '{applocale.lastname}',
        },
    },
    {
        xtype: 'textfield',
        name: 'email',
        required: true,
        reference: 'email',
        bind: {
            label: '{applocale.email}',
        },
    },
    {
        xtype: 'container',
        layout: {
            type: 'hbox',
            pack: 'right',
        },
        items: [
        {
            xtype: 'label',
            bind: {
                html: '{applocale.emailProblem}',
            },
            reference: 'emailWrong',
            hidden: true
        }]
    },
    {
        xtype: 'passwordfield',
        name: 'password',
        required: true,
        reference: 'password',
        bind: {
            label: '{applocale.password}',
        },
    },
    {
        xtype: 'passwordfield',
        name: 'repeatedPassword',
        required: true,
        reference: 'repeatedPassword',
        bind: {
            label: '{applocale.confirm}',
        },
    },
    {
        xtype: 'container',
        layout: {
            type: 'hbox',
            pack: 'right',
        },
        items: [
        {
            xtype: 'label',
            bind: {
                html: '{applocale.passwordProblem}',
            },
            reference: 'passwordWrong',
            hidden: true
        }]
    }],

    buttons: [
    {
        bind: {
            text: '{applocale.back}',
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left',
    },
    {
        bind: {
            text: '{applocale.register}',
        },
        docked: 'right',
        reference: 'submitRegister',
        handler: 'registerHandler',
        iconCls: 'x-fa fa-user-circle',
        iconAlign: 'right',
    }]
});