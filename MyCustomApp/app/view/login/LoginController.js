Ext.define('MyApp.view.login.LoginController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.login',

    backHandler: function () {
        history.back();
        var forma = Ext.getCmp('loginForm');
        forma.reset();
    },

    loginHandler: function () {

        var forma = Ext.getCmp('loginForm');
        forma.validate();

        if (forma.isValid()) {

            Ext.Ajax.request({
                scope: this,
                url: 'http://localhost:8080/users/login',

                params: Ext.encode(this.getView().getValues()),

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                success: function (response) {
                    var username = Ext.getCmp('username').getValue();
                    localStorage.setItem('token1', username);
                    localStorage.setItem('token', "logged");


                    Ext.Ajax.request({
                        url: 'http://localhost:8080/users/'+username,
                        method: 'GET',

                        defaultHeaders: {
                            'Content-Type': 'application/json ',
                        },

                        success: function (response, opts) {
                            var responseAdmin = Ext.decode(response.responseText);
                            var adminIdRes = responseAdmin.id;
                            localStorage.setItem('adminId', adminIdRes);
                            localStorage.setItem('adminIdTest', '3');
                        },

                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });

                    this.redirectTo('projects');
                    forma.reset();
                },

                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        else {
        }
    }
});