Ext.define('MyApp.view.login.Login', {
    extend: 'Ext.form.Panel',

    bind: {
        title: '{applocale.login}'
    },

    id: 'loginForm',
    renderTo: 'formId',
    controller: 'login',
    viewModel: 'login',
    hidden: false,

    xtype: 'login',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',
    //hideAnimation: 'slideOut',

    // header:{
    //     titlePosition: 0,
    // },

    // listeners: {
    //     show: 'onShow'
    // },

    requires: [
        'MyApp.view.login.LoginModel',
        'MyApp.view.login.LoginController',
    ],

    items: [
    {
        xtype: 'textfield',
        bind: {
            label: '{applocale.username}'
        },
        name: 'username',
        maxLength: 16,
        reference: 'username',
        required: true,
        id: 'username',
    },
    {
        xtype: 'passwordfield',
        name: 'password',
        required: true,
        bind: {
            label: '{applocale.password}'
        },
    }],

    buttons: [
    {
        bind: {
            text: '{applocale.back}',
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left',
    },
    {
        name: "submitLogin",
        bind: {
            text: '{applocale.login}'
        },
        docked: 'right',
        reference: 'submitLogin',
        handler: 'loginHandler',
        iconCls: 'x-fa fa-sign-in',
        iconAlign: 'right'
    }],
});