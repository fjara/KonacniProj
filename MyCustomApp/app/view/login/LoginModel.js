Ext.define('MyApp.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.login',

    fields: [{
        name: 'username',
        validators: 'presence'
    },
        {
            name: 'password',
            validators: 'presence'
        }],


    data: {
        applocale: {
            username: "Username",
            password: "Password",
            back: "Back",
            reset: "Reset",
            login: "Login",
            register: "Register"
        }

    }
});