Ext.define('MyApp.view.addTask.AddTaskModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.addTask',


    fields: [{
        name: 'description',
        validators: 'presence'
       },
        {
            name: 'summary',
            validators: 'presence'
        },
        {
            name: 'assignee',
            validators: 'presence'
        }],

    data: {
        applocale: {
            username: "Username",
            password: "Password",
            back: "Back",
            reset: "Reset",
            addTask: "Add Task",
            taskName: "Task Name",
            logout: "",
            summary:"Summary",
            assignee:"Assignee",
            type:"Type",
            storyPoints:"Story Points",
            description:"Description",
            status: "Status",
        }
    },
});