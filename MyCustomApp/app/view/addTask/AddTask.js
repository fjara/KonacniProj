Ext.define('MyApp.view.addTask.AddTask', {
    extend: 'Ext.form.Panel',

    bind: {
        title: '{applocale.addTask}'
    },

    id: 'addTaskForm',
    renderTo: 'addTaskId',
    controller: 'addTask',

    viewModel: 'addTask',
    reference: 'addTask',

    requires: [
        'MyApp.view.addTask.AddTaskModel',
        'MyApp.view.addTask.AddTaskController',
    ],

    xtype: 'addTask',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',

    listeners: {
        show: 'onShow'
    },

    header: {
        titlePosition: 0,
        items: [
        {
            xtype: 'button',
            bind: {
                text: '{applocale.logout}'
            },
            docked: 'right',
            handler: 'logout',
            iconCls: 'x-fa fa-sign-out',
            iconAlign: 'right',
        }]
    },

    items: [
    {
        xtype: 'panel',
        layout: {
            type: 'vbox'
        },
        items: [
        {
            xtype: 'textareafield',
            bind: {
                label: '{applocale.summary}',
            },
            maxLength: 200,
            height: '85px',
            id: 'summary',
            required: true
        },
        {
            xtype: 'combobox',
            bind: {
                label: '{applocale.type}',
            },
            displayField: 'name',
            valueField: 'abbr',
            required: true,
            reference: 'typeCombo',
            store: [
                {abbr: 'TASK', name: 'TASK'},
                {abbr: 'BUG', name: 'BUG'},
                {abbr: 'IMPROVEMENT', name: 'IMPROVEMENT'},
                {abbr: 'NEW_FEATURE', name: 'NEW_FEATURE'}
            ]
        },
        // {
        //     xtype: 'combobox',
        //     bind: {
        //         label: '{applocale.status}',
        //     },
        //     displayField: 'name',
        //     valueField: 'abbr',
        //     required: true,
        //     reference: 'statusCombo',
        //     store: [
        //         {abbr: 'TO_DO', name: 'TO_DO'},
        //         {abbr: 'IN_PROGRESS', name: 'IN_PROGRESS'},
        //         {abbr: 'RESOLVED', name: 'RESOLVED'},
        //     ]
        // },
        {
            xtype: 'combobox',
            bind: {
                label: '{applocale.assignee}',
            },
            displayField: 'name',
            valueField: 'abbr',
            required: true,
            reference: 'assigneeCombo',
        },
        {
            xtype: 'numberfield',
            bind: {
                label: '{applocale.storyPoints}',
            },
            id: 'storyPoints',
            required: true
        },
        {
            xtype: 'textareafield',
            bind: {
                label: '{applocale.description}',
            },
            id: 'description',
            required: true
        },
        {
            xtype: 'label',
            hidden: true,
            reference: 'idProj'
        }]
    }],

    buttons: [
    {
        bind: {
            text: '{applocale.back}',
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left',
    },
    {
        bind: {
            text: '{applocale.addTask}',
        },
        docked: 'right',
        reference: 'addTaskReferHandler',
        handler: 'addHandler',
        iconCls: 'x-fa fa-plus',
        iconAlign: 'right',
    }],
});