Ext.define('MyApp.view.addTask.AddTaskController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.addTask',

    addHandler: function () {
        var forma = Ext.getCmp('addTaskForm');
        var url = window.location.href;
        var urlId = url.split("/");
        var last_element = urlId[urlId.length - 1];

        forma.validate();
        if (forma.isValid()) {

            var token = localStorage.getItem("token1");

            var assignee = this.lookupReference('assigneeCombo').getValue();
            var summary= Ext.getCmp('summary').getValue();
            var descr = Ext.getCmp('description').getValue();
            var idProj = last_element;
            var storyPoints = Ext.getCmp('storyPoints').getValue();;
            var type = this.lookupReference('typeCombo').getValue();
            // var status = this.lookupReference('statusCombo').getValue();
            var status = 'TO_DO';
            var creatorUsername = localStorage.getItem("token1");


            Ext.Ajax.request({
                scope: this,
                url: 'http://localhost:8080/tickets',
                method: 'POST',

                params: Ext.encode({
                    "_usernameAssignee": assignee,
                    "_usernameCreator": creatorUsername,
                    "description": descr,
                    "project_id": idProj,
                    "points": storyPoints,
                    "summary": summary,
                    "ticketType": type,
                    "status": status
                }),

                defaultHeaders: {
                    'Content-Type': 'application/json'
                },

                success: function (response, opts) {
                    var obj = Ext.decode(response.responseText);
                    history.back();
                    forma.reset();
                },

                failure: function (response, opts) {
                    console.log('server-side failure with status code ' + response.status);
                }
            });
        }
        else {
        }
    },

    onShow: function(){
        var assigneeStore = [];

        var storeAddTask = Ext.create('Ext.data.Store',{
            fields : ['abbr','name'],
            data: assigneeStore,
            paging : false,
        });

        Ext.Ajax.request({

            scope: this,
            url: 'http://localhost:8080/users',
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json'
            },

            success: function (response, opts) {
                var usersJson = Ext.decode(response.responseText);

                for (var i = 0; i < usersJson.length; i++){
                    assigneeStore.push({
                        abbr: usersJson[i].username,
                        name: usersJson[i].firstName+' '+usersJson[i].lastName
                    });
                }
                storeAddTask.loadData(assigneeStore);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });

        this.lookupReference('assigneeCombo').setStore(storeAddTask);
    },

    logout: function () {
        localStorage.setItem('token', "notlogged"); //vise nije ulogovan
        localStorage.setItem('token1', "Nepostojeci korisnik");
        this.redirectTo("home");
    },

    backHandler: function () {
        history.back();
        var forma = Ext.getCmp('addTaskForm');
        forma.reset();
    },

});