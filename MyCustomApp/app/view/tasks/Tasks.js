Ext.define('MyApp.view.tasks.Tasks', {
    extend: 'Ext.form.Panel',

    bind: {
        title: '{applocale.tasks}'
    },
    //da mogu push

    id: 'tasks',
    renderTo: 'tasksId',
    controller: 'tasks',
    viewModel: 'tasks',

    xtype: 'tasks',
    height: '100%',
    width: '100%',
    showAnimation: 'slideIn',
    cls: 'task-wrap',

    requires: [
        'MyApp.view.tasks.TasksModel',
        'MyApp.view.tasks.TasksController',
    ],

    listeners: {
        show: 'onShow'
    },

    buttons: [
    {
        bind: {
            text: '{applocale.back}'
        },
        docked: 'left',
        reference: 'back',
        handler: 'backHandler',
        iconCls: 'x-fa fa-long-arrow-left',
        iconAlign: 'left'
    },
    {
        bind: {
            text: '{applocale.add}'
        },
        docked: 'right',
        reference: 'addTaskRefHandler',
        handler: 'addTaskHandler',
        iconCls: 'x-fa fa-plus',
        iconAlign: 'right',
        hidden: true
    }],

    header: {
        titlePosition: 0,
        items: [
        {
            xtype: 'button',
            bind: {
                text: '{applocale.logout}'
            },
            docked: 'right',
            handler: 'logout',
            iconCls: 'x-fa fa-sign-out',
            iconAlign: 'right',
        },
        {
            xtype: 'label',
            reference: 'idProj',
            hidden: true
        }]
    }
});