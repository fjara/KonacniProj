Ext.define('MyApp.view.tasks.TasksController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tasks',

    backHandler: function () {
        history.back();
        //da mogu push
    },

    addTaskHandler: function () {
        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var projName = urlSplitIds[urlSplitIds.length - 1];
        var idProj = urlSplitIds[urlSplitIds.length - 2];
        this.redirectTo('addTask/' + idProj);
    },

    onShow: function () {

        var adminChecker = localStorage.getItem("adminId");
        var adminCheckerNumber = localStorage.getItem("adminIdTest");
        if(adminChecker === adminCheckerNumber){
            this.lookupReference('addTaskRefHandler').setHidden(false);
        } else {
            this.lookupReference('addTaskRefHandler').setHidden(true);
        }

        var token = localStorage.getItem("token1");
        var c = this.lookupReference('userLogged');

        if (c == "Nepostojeci korisnik") { //ako niko nije ulogovan a neko je preko linka u browseru otisao na projects
            this.redirectTo('home', false);
        }

        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var projName = urlSplitIds[urlSplitIds.length - 1];
        var idProj = urlSplitIds[urlSplitIds.length - 2];

        Ext.Ajax.request({

            url: 'http://localhost:8080/projects/'+projName+'/tickets',
            method: 'GET',

            defaultHeaders: {
                'Content-Type': 'application/json '
            },

            success: function (response, opts) {
                var forma = Ext.getCmp('tasks');
                forma.removeAll();
                var response = Ext.decode(response.responseText);

                var children = [];

                Ext.each(response, function (dataItem) {

                    Ext.Ajax.request({
                        async: false,
                        url: 'http://localhost:8080/tickets/'+dataItem.id+'/userAssignee',
                        method: 'GET',

                        defaultHeaders: {
                            'Content-Type': 'application/json '
                        },

                        success: function (response, opts) {
                            var obj = Ext.decode(response.responseText);
                            assigneeCurrent = obj.firstName+' '+obj.lastName;
                            localStorage.setItem('currentAssigneTask', assigneeCurrent);
                        },

                        failure: function (response, opts) {
                            console.log('server-side failure with status code ' + response.status);
                        }
                    });

                    var assigneeCurrent = localStorage.getItem("currentAssigneTask");

                    //create child
                    children.push({

                        xtype: 'panel',
                        items: [
                        {
                            xtype: 'panel',
                            cls: 'taskClass'+dataItem.ticketType,
                            layout: {
                                type: 'vbox'
                            },
                            items: [
                            {
                                xtype: 'label',
                                html: dataItem.summary,
                                cls: dataItem.ticketType
                            },
                            {
                                xtype: 'panel',
                                layout: {
                                    type: 'hbox',
                                    pack: 'space-between'
                                },
                                items: [
                                {
                                    xtype: 'label',
                                    html: assigneeCurrent
                                },
                                {
                                    xtype: 'label',
                                    html: dataItem.ticketType
                                }]
                            },
                            {
                                xtype: 'panel',
                                layout: {
                                    type: 'hbox',
                                    pack: 'center'
                                },
                                items:[
                                {
                                    xtype: 'button',
                                    bind: {
                                        text: '{applocale.details}'
                                    },
                                    reference: 'taskDetButton '+dataItem.id,
                                    handler: 'onClick',
                                    iconCls: 'x-fa fa-long-arrow-right',
                                    iconAlign: 'right'
                                }]
                            }]
                        }]
                    });
                });
                //add children to panel at once
                forma.add(children);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    logout: function () {
        localStorage.setItem('token', "notlogged"); //vise nije ulogovan
        localStorage.setItem('token1', "Nepostojeci korisnik");
        this.redirectTo("home");
    },

    onClick: function(dugme){
        var url = window.location.href;
        var urlSplit = url.split("/");
        var urlSplitIds = urlSplit[urlSplit.length - 1].split("_");
        var projName = urlSplitIds[urlSplitIds.length - 1];
        var idProj = urlSplitIds[urlSplitIds.length - 2];

        var taskDugmeReference = dugme.getReference();
        var taskDugmeReferenceSplit = taskDugmeReference.split(' ');
        var idTask = taskDugmeReferenceSplit[taskDugmeReferenceSplit.length - 1];

        this.redirectTo('tasksDetails/'+idProj+'_'+projName+'_'+idTask);
    }
});