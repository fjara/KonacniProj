Ext.define('MyApp.view.tasks.TasksModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tasks',

    data: {
        applocale: {
            username: "Username",
            password: "Password",
            back: "Back",
            reset: "Reset",
            projects: "Projects",
            add: "Add Task",
            logout: "",
            tasks:"Tasks",
            details:"Details",
            //da mogu push
        }
    }
});