Ext.define('MyApp.view.home.HomeController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.home',

    loginHome: function(){
        this.redirectTo('login', false);
    },
    registerHome: function(){
        this.redirectTo('register',false);
    },

    onShow: function(){
        Ext.getCmp('home').setShowAnimation('slideIn');
        console.log(Ext.getCmp('home').getShowAnimation());
    }
});