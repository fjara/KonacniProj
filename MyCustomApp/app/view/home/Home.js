Ext.define('MyApp.view.home.Home', {
    extend: 'Ext.Panel',

    bind: {
        title: '{applocale.projectManager}'
    },

    id: 'home',
    //reference: 'homeScreen',
    renderTo: 'homeId',
    controller: 'home',

    viewModel: 'home',
    iconCls: 'x-fa fa-user',
    iconAlign: 'right',

    requires: [
        'MyApp.view.home.HomeModel',
        'MyApp.view.home.HomeController',
    ],

    xtype: 'home',
    height: '100%',
    width: '100%',
    showAnimation: null,

    listeners: {
        show: 'onShow'
    },

    items: [
    {
        xtype: 'container',
        layout: {
            type: 'vbox',
            //align: 'center'
        },
        items: [
        {
            xtype: 'image',
            src: 'resources/project.png',
            cls: 'home-page-background-image'
        },
        {
            xtype: 'container',
            layout: {
                type: 'vbox',
            },
            items: [
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'space-around',
                },
                items: [
                    {
                        xtype: 'button',
                        bind: {
                            text: '{applocale.login}'
                        },
                        reference: 'loginHome',
                        handler: 'loginHome',
                        iconCls: 'x-fa fa-sign-in',
                        iconAlign: 'right',
                        cls: 'home-page-button',
                    },
                    {
                        xtype: 'button',
                        bind: {
                            text: '{applocale.register}'
                        },
                        iconCls: 'x-fa fa-user-circle',
                        iconAlign: 'right',
                        reference: 'registerHome',
                        handler: 'registerHome',
                        cls: 'home-page-button'
                    }
                    ]
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'right'
                },
            }]
        }]
    }]
});