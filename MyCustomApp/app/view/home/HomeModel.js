Ext.define('MyApp.view.home.HomeModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.home',


    data: {
        applocale: {
            login: "Login",
            register: "Register",
            projectManager: "Project Manager",
        }

    }


});