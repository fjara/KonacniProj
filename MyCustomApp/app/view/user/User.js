Ext.define('MyApp.view.user.User', {
    extend: 'Ext.form.Panel',
    title: 'Start Page',
    id: 'newForm',
    renderTo: 'formId',
    controller: 'user',
    viewModel: 'user',

    requires: [
        'MyApp.view.user.UserModel',
        'MyApp.view.user.UserController',


    ],
    xtype: 'user',
    items: [
        {   xtype: 'textfield',
            name:'welcome',
            label: 'welcome User'}
]
});